package com.devcamp.drinkapi.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.devcamp.drinkapi.model.Drink;

@Service
public class DrinkService {
    Drink drink1 = new Drink("COCACOLA", "Cocacola", 10000, null, new Date(), new Date());
    Drink drink2 = new Drink("PEPSI", "Pepsi", 30000, null, new Date(), new Date());
    Drink drink3 = new Drink("C2", "C2", 20000, null, new Date(), new Date());
    Drink drink4 = new Drink("STING", "Sting", 250000, null, new Date(), new Date());
    Drink drink5 = new Drink("LARUE", "Larue", 310000, null, new Date(), new Date());
    Drink drink6 = new Drink("LAVIE", "Lavie", 170000, null, new Date(), new Date());

    public ArrayList<Drink> allDrinks() {

        ArrayList<Drink> drinks = new ArrayList<>();
        drinks.add(drink1);
        drinks.add(drink2);
        drinks.add(drink3);
        drinks.add(drink4);
        drinks.add(drink5);
        drinks.add(drink6);
        return drinks;

    }
}
